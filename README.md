# OpenML dataset: Lichess_Games_Dataset

https://www.openml.org/d/45951

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The dataset games.csv is a comprehensive collection of chess game records, providing detailed insights into game outcomes, player ratings, moves, and opening strategies. It consists of several attributes designed to analyze game dynamics and player performance in chess matches.

Attribute Description:
- **id:** A unique identifier for each game represented by strings like 'JRpms95z'.
- **rated:** A boolean indicating if the game was rated (True) or not.
- **created_at:** The timestamp when the game was created, in milliseconds.
- **last_move_at:** The timestamp for the last move made in the game.
- **turns:** The total number of turns taken in the game.
- **victory_status:** The method by which the game was won ('mate', 'resign', etc.).
- **winner:** The side that won the game ('white', 'black', or 'draw').
- **increment_code:** Time control settings for the game, represented in a string like '7+9'.
- **white_id:** The username of the player controlling the white pieces.
- **white_rating:** The rating of the white player at the time of the game.
- **black_id:** The username of the player controlling the black pieces.
- **black_rating:** The rating of the black player at the time of the game.
- **moves:** The sequence of moves made during the game, recorded in standard chess notation.
- **opening_eco:** The Encyclopaedia of Chess Openings (ECO) code for the game's opening.
- **opening_name:** The name of the opening played, like "King's Indian Defense".
- **opening_ply:** The number of moves in the opening phase.

Use Case:
This dataset is invaluable for chess enthusiasts, researchers, and developers interested in developing chess-related algorithms, studying patterns and trends in game outcomes and strategies, or creating predictive models on game results based on player ratings and opening moves. It can also be used to analyze the effectiveness of different openings or to train machine learning models to forecast game outcomes from early game data.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45951) of an [OpenML dataset](https://www.openml.org/d/45951). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45951/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45951/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45951/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

